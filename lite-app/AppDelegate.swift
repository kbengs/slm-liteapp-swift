//
//  AppDelegate.swift
//  lite-app
//
//  Created by Kari on 10-02-2017.
//  Copyright © 2017 Adeprimo. All rights reserved.
//

import UIKit
import Batch

var BatchAPIKey = ""
let UserNotificationChanged = "userNotificationSettingsChanged"
var BatchInstallationID = ""

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    let notificationDelegate = NotificationDelegate()

    var window: UIWindow?
    var loadUrl: String?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        if #available(iOS 10, *) {
            UNUserNotificationCenter.current().delegate = notificationDelegate
        }
        
        self.setupBatch()
        
        let subscriptionManager = SubscriptionManager()
        if !subscriptionManager.didSyncWithBatch {
            subscriptionManager.initDefaults()
        }
        subscriptionManager.resetOneSignal()
        
        return true
    }
    
    func setupBatch() {
        let bundle = Bundle.main
        if let info = bundle.infoDictionary,
            let batchAPIKey = info["BatchAPIKey"] as? String {
            if !batchAPIKey.isEmpty {
                Batch.start(withAPIKey: batchAPIKey)
            
                let pushManager = PushManager()
                pushManager.register()
            }
            else {
                print ("BatchAPIKey is missing")
            }
        }
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.

    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.

    }
    
    func application(_ application: UIApplication, didRegister notificationSettings: UIUserNotificationSettings) {
        NotificationCenter.default.post(name: Notification.Name(rawValue: UserNotificationChanged), object: nil)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {

        if let url = userInfo["url"] as? String {
            self.loadUrl = url
            let notificationName = Notification.Name("updateWebViewUrl")
            NotificationCenter.default.post(name: notificationName, object: nil)
            //ViewController.loadUrl(url: url)
        }
        
        completionHandler(UIBackgroundFetchResult.noData)
    }

}

