//
//  NotificationCell.swift
//  lite-app
//
//  Created by Kari on 16-02-2017.
//  Copyright © 2017 Adeprimo. All rights reserved.
//

import UIKit

class NotificationCell: UITableViewCell {
    
    @IBOutlet weak var notificationLabel: UILabel!
    
    @IBOutlet weak var notificationEnabledSwitch: UISwitch!
    
    var notification: PushCategory?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Disable the switch until a source is added to avoid a deceptive UI
        notificationEnabledSwitch.isEnabled = false
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
    }

    func update(_ notification: PushCategory) {
        self.notification = notification
        notificationEnabledSwitch.isEnabled = true
        notificationEnabledSwitch.isOn = SubscriptionManager().isSubscribedToCategory(notification.tag)
    }
    
    @IBAction func notificationToggledAction(_ sender: Any) {
        if let notification = notification {
            SubscriptionManager().togglePushCategory(notification.tag, enabled: notificationEnabledSwitch.isOn)
        }
    }

}
