//
//  NotificationDelegate.swift
//  lite-app
//
//  Created by Kari on 22-02-2017.
//  Copyright © 2017 Adeprimo. All rights reserved.
//

import Foundation
import Batch


@objc
class NotificationDelegate: NSObject, UNUserNotificationCenterDelegate {
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        let userInfo = response.notification.request.content.userInfo
        if let url = userInfo["url"] as? String {
            let delegate = UIApplication.shared.delegate as? AppDelegate
            delegate?.loadUrl = url
            let notificationName = Notification.Name("updateWebViewUrl")
            NotificationCenter.default.post(name: notificationName, object: nil)
            //ViewController.loadUrl(url: url)
        }
        BatchPush.handle(userNotificationCenter: center, didReceive: response)
        completionHandler()
    }
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        BatchPush.handle(userNotificationCenter: center, willPresent: notification, willShowSystemForegroundAlert: true)
        completionHandler([.alert, .sound])
        
        // Since you set willShowSystemForegroundAlert to true, you should call completionHandler([.alert, .sound, .badge])
        // If set to false, you'd call completionHandler([])
    }
}
