//
//  PushManager.swift
//  lite-app
//
//  Created by Kari on 20-02-2017.
//  Copyright © 2017 Adeprimo. All rights reserved.
//

import UserNotifications
import Batch.Push

class PushManager {
    
    func register() {
        BatchPush.registerForRemoteNotifications()
        BatchInstallationID = BatchUser.installationID()!
    }
    
}
