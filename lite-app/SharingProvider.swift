//
//  SharingProvider.swift
//  lite-app
//
//  Created by Kari on 15-02-2017.
//  Copyright © 2017 Adeprimo. All rights reserved.
//

import UIKit

class SharingProvider: UIActivityItemProvider {
    var subject: String
    var text: String
    var url: URL
    var sharetag: String?
    
    init(subject: String, text: String, url: URL, sharetag: String? = nil) {
        self.subject = subject
        self.text = text
        self.url = url
        
        let bundle = Bundle.main
        if let info = bundle.infoDictionary {
            if let sharetag = info["ShareTag"] as? String {
                self.sharetag = sharetag
            }
        }
        
        super.init(placeholderItem: "")
    }
    
    override func activityViewController(_ activityViewController: UIActivityViewController, subjectForActivityType activityType: UIActivityType?) -> String {
        return subject
    }
    
    override func activityViewController(_ activityViewController: UIActivityViewController, itemForActivityType activityType: UIActivityType) -> Any? {
        if activityType == UIActivityType.postToTwitter {
            if (sharetag != nil) {
                return text + " " + sharetag! + " " + url.absoluteString
            }
        }
        else if activityType == UIActivityType.message {
            return url.absoluteString
        }
        
        return text + " " + url.absoluteString
    }
    
}
