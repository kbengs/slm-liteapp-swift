//
//  SubscriptionManager.swift
//  lite-app
//
//  Created by Kari on 20-02-2017.
//  Copyright © 2017 Adeprimo. All rights reserved.
//

import Foundation
import Batch.User

private let pushCategories = "push_categories"

class SubscriptionManager {
    
    var automaticBatchSync: Bool = true
    
    var didSyncWithBatch: Bool {
        get {
            return UserDefaults.standard.bool(forKey: "syncWithBatch")
        }
        
        set {
            UserDefaults.standard.set(newValue, forKey: "syncWithBatch")
        }
    }
    
    
    func isSubscribedToCategory(_ name: String) -> Bool {
        
        if let subscribedCategories = UserDefaults.standard.stringArray(forKey: pushCategories) {
            return subscribedCategories.index(of: name) != nil
        }
        
        return false
    }
    
    func togglePushCategory(_ name: String, enabled: Bool) {
        toggleArrayValue(pushCategories, value: name, enabled: enabled)
    }
    
    fileprivate func toggleArrayValue(_ arrayName: String, value: String, enabled: Bool) {
        let defaults = UserDefaults.standard
        var array: [String] = defaults.stringArray(forKey: arrayName) ?? []
        
        if enabled {
            if array.index(of: value) == nil {
                array.append(value)
            }
        } else {
            array = array.filter({ (arrayItem) -> Bool in
                arrayItem != value
            })
        }
        
        defaults.setValue(array, forKey: arrayName)
        
        if automaticBatchSync {
            syncDataWithBatch()
        }
    }
    
    // Sets the default subscriptions from One Signal
    func initDefaults() {
        let defaults = UserDefaults.standard
        var array: [String] = defaults.stringArray(forKey: pushCategories) ?? []
        print ("Syncing with Batch")
        let categories = PushCategory.loadFromPlist()
        
        categories.forEach({ (category) in
            let enabled = UserDefaults.standard.object(forKey: "push_" + category.tag) as? Bool ?? true
            if enabled {
                if array.index(of: category.tag) == nil {
                    array.append(category.tag)
                }
            }
            //print ("Syncing tag: " + category.tag + " value: " + propValue.description)
            //editor.setAttribute(NSNumber(value: propValue), forKey:category.tag)
            //UserDefaults.standard.setValue(propValue, forKey: "push_" + category.tag)
            //editor.removeAttribute(forKey: category.tag)
        })
        defaults.setValue(array, forKey: pushCategories)
        syncDataWithBatch()
        didSyncWithBatch = true
        
    }
    
    func resetOneSignal() {
        let defaults = UserDefaults.standard
        if let playerId = defaults.object(forKey: "OneSignal.Player.Id") as? String {
            
            let bundle = Bundle.main
            if let info = bundle.infoDictionary,
                let oneSignalAppId = info["OneSignalAppId"] as? String {
                if !oneSignalAppId.isEmpty {
                    
                    let payload: [String: Any] = ["app_id": oneSignalAppId,
                                                  "tags": ["nyheter":"", "sport":"", "kultur":""]]
                    //print(playerId)
                    let playersEndpoint = URL(string: "https://onesignal.com/api/v1/players/" + playerId)
                    var oneSignalRequest = URLRequest(url: playersEndpoint!)
                    oneSignalRequest.httpMethod = "PUT"
                    oneSignalRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
                    
                    let json: Data
                    do {
                        json = try JSONSerialization.data(withJSONObject: payload, options: [])
                        oneSignalRequest.httpBody = json
                    } catch {
                        print("Error: cannot create JSON")
                        return
                    }
                    
                    let session = URLSession.shared
                    
                    let task = session.dataTask(with: oneSignalRequest) {
                        (data, response, error) in
                        guard error == nil else {
                            print("error calling PUT")
                            return
                        }
                        /*guard let responseData = data else {
                         print("Error: did not receive data")
                         return
                         }*/
                        
                        defaults.removeObject(forKey: "OneSignal.Player.Id")
                         //print (String(data: responseData, encoding: String.Encoding.utf8) as String!)*/
                        
                    }
                    task.resume()
                }
                
            }
        }
        
    }
    
    // Synchronise data in NSUserDefaults with Batch
    func syncDataWithBatch() {
        let defaults = UserDefaults.standard
        
        let editor = BatchUser.editor()
        
        editor.clearTagCollection(pushCategories)
        defaults.stringArray(forKey: pushCategories)?.forEach({ (optin) in
            editor.addTag(optin, inCollection: pushCategories)
        })
        
        editor.save()
    }
    
    
}
